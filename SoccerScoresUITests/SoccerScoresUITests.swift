//
//  SoccerScoresUITests.swift
//  SoccerScoresUITests
//
//  Created by Mateo Quiceno Sosa on 31/03/24.
//

import XCTest

@testable import SoccerScores

final class SoccerScoresUITests: XCTestCase {

    override class var runsForEachTargetApplicationUIConfiguration: Bool {
        true
    }

    override func setUpWithError() throws {
        continueAfterFailure = false
    }

    func testLaunch() throws {
        let app = XCUIApplication()
        app.launch()

        // Insert steps here to perform after app launch but before taking a screenshot,
        // such as logging into a test account or navigating somewhere in the app

        let attachment = XCTAttachment(screenshot: app.screenshot())
        attachment.name = "Launch Screen"
        attachment.lifetime = .keepAlways
        add(attachment)
    }
    
    func testValidateNavigationLinkSelectLeaguesView() throws {
        let app = XCUIApplication()
        app.launch()
        
        XCTAssertNotNil(app.otherElements["Select league"].firstMatch.label)
        XCTAssertNotNil(app.otherElements["listTeams"])
        app.terminate()
    }
    
    func testValidateNavigateToSelectLeaguesView() throws {
        let app = XCUIApplication()
        app.launch()
        XCUIDevice.shared.orientation = .portrait
        
        sleep(2)
        
        let navigationLink = app.otherElements["Select league"]
        navigationLink.tap()
        
        XCTAssertNotNil(app.staticTexts["nameLeague"].identifier)
        XCTAssertNotNil(app.staticTexts["nameLeagueAlternate"].identifier)
        
        app.terminate()
    }
    
    func testValidateSwipeHorizontalLeaguesView() throws {
        let app = XCUIApplication()
        app.launch()
        XCUIDevice.shared.orientation = .portrait
        
        sleep(2)
        
        let navigationLink = app.otherElements["Select league"]
        navigationLink.tap()
        
        app.swipeLeft(velocity: .slow)
        app.swipeLeft(velocity: .slow)
        app.swipeLeft(velocity: .slow)
        
        XCTAssertNotNil(app.staticTexts["nameLeague"].identifier)
        XCTAssertNotNil(app.staticTexts["nameLeagueAlternate"].identifier)
        
        app.terminate()
    }
    
    func testValidateSelectLeagueLeaguesView() throws {
        let app = XCUIApplication()
        app.launch()
        XCUIDevice.shared.orientation = .portrait
        
        sleep(2)
        
        let navigationLink = app.otherElements["Select league"]
        navigationLink.tap()
        
        app.swipeLeft(velocity: .slow)
        app.swipeLeft(velocity: .slow)
        
        let button = app.buttons["selectLeague"]
        button.tap()
        
        XCTAssertNotNil(app.otherElements["Select league"].firstMatch.label)
        XCTAssertNotNil(app.otherElements["listTeams"])

        app.terminate()
    }
    
    func testDisplayListAndGridTeamsView() throws {
        let app = XCUIApplication()
        app.launch()
        XCUIDevice.shared.orientation = .portrait
        
        sleep(2)
        
        let buttonGrid = app.buttons["buttonGridTeams"]
        buttonGrid.tap()
        
        XCTAssertNotNil(app.otherElements["gridViewTeams"])
        
        let buttonList = app.buttons["buttonListTeams"]
        buttonList.tap()
        
        XCTAssertNotNil(app.otherElements["listTeams"])
        
        app.terminate()
    }
    
    func testItemListOpenDetailTeamView() throws {
        let app = XCUIApplication()
        app.launch()
        XCUIDevice.shared.orientation = .portrait
        
        sleep(2)
        
        let text = app.staticTexts["nameTeamList"].firstMatch
        text.tap()
        
        app.swipeUp(velocity: .slow)
        
        XCTAssertNotNil(app.navigationBars.staticTexts.firstMatch.label)
        XCTAssertNotNil(app.otherElements["imageTeamLogo"])
        XCTAssertNotNil(app.otherElements["imageTeamBadge"])

        app.terminate()
    }
    
    func testValidateInfoStadiumDetailTeamView() throws {
        let app = XCUIApplication()
        app.launch()
        XCUIDevice.shared.orientation = .portrait
        
        sleep(2)
        
        let text = app.staticTexts["nameTeamList"].firstMatch
        text.tap()
        
        app.swipeUp(velocity: .slow)
        
        let view = app.otherElements["stadiumDetail"]
        view.tap()
        
        app.swipeUp(velocity: .slow)
        
        sleep(2)
        
        XCTAssertNotNil(app.otherElements["stadiumThumb"])
        XCTAssertNotNil(app.otherElements["stadiumLocation"])
        XCTAssertNotNil(app.otherElements["stadiumCapactiy"])
        XCTAssertNotNil(app.otherElements["stadiumDescription"])

        app.terminate()
    }
    
    func testValidateMoreAboutTeamView() throws {
        let app = XCUIApplication()
        app.launch()
        XCUIDevice.shared.orientation = .portrait
        
        sleep(2)
        
        let text = app.staticTexts["nameTeamList"].firstMatch
        text.tap()
        
        app.swipeUp(velocity: .slow)
        
        let button = app.buttons["moreAboutTeam"]
        button.tap()
        
        app.swipeUp(velocity: .slow)
        
        sleep(2)
        
        XCTAssertNotNil(app.otherElements["keywordsTeam"])
        XCTAssertNotNil(app.otherElements["descriptionTeam"])

        app.terminate()
    }
    
    func testValidateResultsViewTeamView() throws {
        let app = XCUIApplication()
        app.launch()
        XCUIDevice.shared.orientation = .portrait
        
        sleep(2)
        
        let text = app.staticTexts["nameTeamList"].firstMatch
        text.tap()
        
        app.swipeUp(velocity: .slow)
        
        let view = app.otherElements["resultsView"]
        view.tap()
        
        sleep(2)
        
        XCTAssertNotNil(app.otherElements["eventThumb"])
        XCTAssertNotNil(app.otherElements["eventVideo"])
        
        app.terminate()
    }
}
