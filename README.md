# SoccerScores
Proyecto creado con SwiftUI en el cual podrás visualizar las mejores ligas e información de equipos de futbol.


# Descripción
Podrás visualizar la mejor información acerca de futbol, visualización de ligas de futbol más comunes, ver el detalle del equipo con los últimos resultados de local, detalles sobre su estadio, descricpción y redes sociales.


# Arquitectura
SoccerScores es implementado usando el patrón arquitectónico <strong>Model-View-ViewModel (MVVM)</strong> con el framework Combine que sirve para que la app sea de forma reactiva y en la capacidad de trabajar con eventos asíncronos. Además utiliza CoreData para almacenamiento local en el dispositivo.

# Empezando
1. Descarga el proyecto git clone https://gitlab.com/mateoquiceno/soccerinfo-ios.git<br>
2. Abrir el proyecto en Xcode.<br>
3. Compilar proyecto en emulador.<br>


# Correr pruebas
SoccerScores puede ser probado utilizando pruebas automatizadas que garantizan el correcto comportamiento de la App hacía el usuario final y pruebas unitarias que valida el correcto funcionamiento de un bloque de código

* Pruebas automatizadas: abrir SoccerScoresUITests y correr desde el archivo
* Pruebas unitarias: abrir SoccerScoresTests y correr desde el archivo


# API 
* Apis utilizadas
1. https://www.thesportsdb.com/api/v1/json/3/all_leagues.php
2. https://www.thesportsdb.com/api/v1/json/3/search_all_teams.php
3. https://www.thesportsdb.com/api/v1/json/3/eventslast.php


# Nota
* La app corre en iOS 14, tiene mayor compatibilidad con SwiftUI y utilizando esta versión no se deja de soportar ningun dispositivo que pueda tener iOS 13, ya que si un dispositivo corre iOS 13 puede ser actualizado a iOS 14 :D

