//
//  ResultDetailView.swift
//  SoccerScores
//
//  Created by Mateo Quiceno Sosa on 29/03/24.
//

import SwiftUI
import AVKit
import WebKit

struct ResultDetailView: View {
    
    let eventTeam: EventTeam
    @ObservedObject var imageDetailLoader = ImageLoader()
    @State var imageDetail: UIImage = UIImage()

    var body: some View {
        VStack {
            Image(uiImage: imageDetail)
                .resizable()
                .scaledToFit()
                .onReceive(imageDetailLoader.$image) { image in
                    self.imageDetail = image
                }
                .onAppear {
                    imageDetailLoader.loadImage(for: eventTeam.strThumb ?? String())
                }
                .accessibilityIdentifier(AccesibilityIdentifier.eventThumb.rawValue)
            ResultsCardView(eventTeam: eventTeam)
            Group {
                HStack {
                    Image(systemName: "play.rectangle.on.rectangle")
                        .foregroundColor(.accentColor)
                        .imageScale(.medium)
                    
                    Text(Strings.textResumen.rawValue)
                        .font(.footnote)
                        .fontWeight(.bold)
                }
                YouTubePlayer(url: eventTeam.strVideo ?? String())
                    .frame(height: 200)
                    .cornerRadius(8)
                    .padding(.horizontal)
                    .accessibilityIdentifier(AccesibilityIdentifier.eventVideo.rawValue)
            }
            .padding(.top, 16)
            
            Spacer()
        }
    }
}

// MARK: - PREVIEW

struct ResultDetailView_Previews: PreviewProvider {
    
    static var previews: some View {
        ResultDetailView(eventTeam: eventTeamData[0])
    }
}


