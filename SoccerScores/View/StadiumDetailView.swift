//
//  StadiumDetailView.swift
//  SoccerScores
//
//  Created by Mateo Quiceno Sosa on 30/03/24.
//

import SwiftUI

struct StadiumDetailView: View {
    
    let itemTeamEntity: TeamEntity?
    
    @ObservedObject private var imageLoader = ImageLoader()
    @State private var image: UIImage = UIImage()
    
    var body: some View {
        Text(Strings.titleStadium.rawValue)
            .font(.footnote)
        GroupBox {
            DisclosureGroup(itemTeamEntity?.strStadium ?? String()) {
                VStack {
                    Image(uiImage: image)
                        .resizable()
                        .scaledToFit()
                        .frame(height: 120)
                        .onReceive(imageLoader.$image) { image in
                            self.image = image
                        }
                        .onAppear {
                            imageLoader.loadImage(for: itemTeamEntity?.strStadiumThumb ?? String())
                        }
                        .padding()
                        .accessibilityIdentifier(AccesibilityIdentifier.stadiumThumb.rawValue)
                    
                    Text(Strings.textLocationStadium.rawValue + (itemTeamEntity?.strStadiumLocation ?? String()))
                        .font(.headline)
                        .bold()
                        .accessibilityIdentifier(AccesibilityIdentifier.stadiumLocation.rawValue)
                    
                    Text(Strings.textCapacityStadium.rawValue + (itemTeamEntity?.intStadiumCapacity ?? String()))
                        .font(.headline)
                        .bold()
                        .padding(2)
                        .accessibilityIdentifier(AccesibilityIdentifier.stadiumCapactiy.rawValue)
                    
                    Text(itemTeamEntity?.strStadiumDescription ?? String())
                        .font(.footnote)
                        .accessibilityIdentifier(AccesibilityIdentifier.stadiumDescription.rawValue)
                }
            }
            .font(.headline)
            .foregroundColor(Color(UIColor.label))
        }
    }
}
