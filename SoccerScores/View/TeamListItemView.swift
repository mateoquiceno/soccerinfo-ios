//
//  TeamListItemView.swift
//  SoccerScores
//
//  Created by Mateo Quiceno Sosa on 29/03/24.
//

import SwiftUI

struct TeamListItemView: View {
    
    let team: Team
    @ObservedObject var imageLoader = ImageLoader()
    @State var image: UIImage = UIImage()
    
    var body: some View {
        HStack(alignment: .center, spacing: 16) {
            Image(uiImage: image)
            .resizable()
            .scaledToFill()
            .frame(width: 80, height: 80)
            .cornerRadius(8)
            .onReceive(imageLoader.$image) { image in
                self.image = image
            }
            .onAppear {
                imageLoader.loadImage(for: team.strTeamBadge ?? String())
            }

            VStack(alignment: .leading, spacing: 8) {
                Text(team.strTeam ?? String())
                    .font(.title2)
                    .fontWeight(.heavy)
                    .accessibilityIdentifier(AccesibilityIdentifier.nameTeamList.rawValue)
                
                Text(team.strStadium ?? String())
                    .multilineTextAlignment(.leading)
                    .font(.title3)
                    .lineLimit(2)
            }
        }
    }
}

// MARK: - PREVIEW

struct TeamListItemView_Previews: PreviewProvider {
  static var previews: some View {
      TeamListItemView(team: teamData[0])
  }
}

