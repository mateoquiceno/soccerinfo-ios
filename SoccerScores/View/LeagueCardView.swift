//
//  LeagueCardView.swift
//  SoccerScores
//
//  Created by Mateo Quiceno Sosa on 28/03/24.
//

import SwiftUI

struct LeagueCardView: View {
    
    var league: League
    @Binding var isActive: Bool
    
    @AppStorage("leagueSelected") var leagueSelected: String?
    
    @State private var isAnimating: Bool = false
        
    var body: some View {
        VStack(spacing: 20) {
            Image(systemName: "soccerball")
                .resizable()
                .frame(width: 200, height: 200)
                .shadow(color: Color.gray, radius: 2, x: 2, y: 8)
                .scaleEffect(isAnimating ? 1.0 : 0.6)
                .padding(16)
            
            Text(league.strLeague ?? String())
                .foregroundColor(Color(UIColor.label))
                .font(.largeTitle)
                .fontWeight(.heavy)
                .multilineTextAlignment(.center)
                .padding(.horizontal, 16)
                .shadow(color: Color.gray, radius: 2, x: 2, y: 2)
                .accessibilityIdentifier(AccesibilityIdentifier.nameLeague.rawValue)
            
            Text(league.strLeagueAlternate ?? String())
                .foregroundColor(Color(UIColor.label))
                .multilineTextAlignment(.center)
                .padding(.horizontal, 16)
                .frame(maxWidth: 480)
                .accessibilityIdentifier(AccesibilityIdentifier.nameLeagueAlternate.rawValue)

            Button(action: {
                leagueSelected = league.strLeague
                isActive = false
            }) {
              HStack(spacing: 8) {
                  Text(Strings.buttonSelect.rawValue)
                
                Image(systemName: "figure.soccer")
                  .imageScale(.large)
              }
              .padding(.horizontal, 16)
              .padding(.vertical, 10)
              .background(
                Capsule().strokeBorder(Color(UIColor.label), lineWidth: 1.25)
              )
            }
            .accessibilityIdentifier(AccesibilityIdentifier.selectLeague.rawValue)
            .accentColor(Color(UIColor.label))
        }
        .onAppear {
            withAnimation(.easeOut(duration: 0.5)) {
                isAnimating = true
            }
        }
        .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: .infinity, alignment: .center)
        .background(LinearGradient(gradient: Gradient(colors: [Color(UIColor.systemBackground), .green]), startPoint: .top, endPoint: .bottom))
        .cornerRadius(20)
        .padding(.horizontal, 20)
    }
}
