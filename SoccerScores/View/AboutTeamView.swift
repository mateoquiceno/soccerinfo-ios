//
//  AboutTeamView.swift
//  SoccerScores
//
//  Created by Mateo Quiceno Sosa on 29/03/24.
//

import SwiftUI

struct AboutTeamView: View {
    
    let itemTeamEntity: TeamEntity?
    
    @ObservedObject var imageLoader = ImageLoader()
    @State var image: UIImage = UIImage()
    @ObservedObject var imageLoaderJersey = ImageLoader()
    @State var imageJersey: UIImage = UIImage()
    
    var body: some View {
        ScrollView(.vertical, showsIndicators: false) {
            Text(itemTeamEntity?.strKeywords ?? String())
                .font(.system(size: 50, weight: .bold))
                .multilineTextAlignment(.center)
                .foregroundColor(Color(UIColor.label))
                .accessibilityIdentifier(AccesibilityIdentifier.keywordsTeam.rawValue)
            
            HStack {
                Image(uiImage: image)
                    .resizable()
                    .scaledToFit()
                    .frame(height: 150)
                    .onReceive(imageLoader.$image) { image in
                        self.image = image
                    }
                    .onAppear {
                        imageLoader.loadImage(for: itemTeamEntity?.strTeamBadge ?? String())
                    }
                
                Image(uiImage: imageJersey)
                    .resizable()
                    .scaledToFit()
                    .frame(height: 150)
                    .onReceive(imageLoaderJersey.$image) { image in
                        self.imageJersey = image
                    }
                    .onAppear {
                        imageLoaderJersey.loadImage(for: itemTeamEntity?.strTeamJersey ?? String())
                    }
            }
            
            Text(Strings.textFounded.rawValue + (itemTeamEntity?.intFormedYear ?? String()))
                .font(.title3)
                .bold()
                .padding(.top)
            
            
            Text(itemTeamEntity?.strDescriptionEN ?? String())
                .font(.headline)
                .padding()
                .accessibilityIdentifier(AccesibilityIdentifier.descriptionTeam.rawValue)
        }
    }
}
