//
//  TeamGridItemView.swift
//  SoccerScores
//
//  Created by Mateo Quiceno Sosa on 29/03/24.
//

import SwiftUI

struct TeamGridItemView: View {
    
    let team: Team
    @ObservedObject var imageLoader = ImageLoader()
    @State var image: UIImage = UIImage()
    
    var body: some View {
        VStack {
            Image(uiImage: image)
                .resizable()
                .resizable()
                .scaledToFit()
                .cornerRadius(8)
                .onReceive(imageLoader.$image) { image in
                    self.image = image
                }
                .onAppear {
                    imageLoader.loadImage(for: team.strTeamBadge ?? String())
            }
            Text(team.strTeam ?? String())
                .font(.title3)
                .foregroundColor(Color(UIColor.label))
                .bold()
        }
    }
}

  // MARK: - PREVIEW

  struct TeamGridItemView_Previews: PreviewProvider {
    
    static var previews: some View {
        TeamGridItemView(team: teamData[0])
        .previewLayout(.sizeThatFits)
        .padding()
    }
  }
