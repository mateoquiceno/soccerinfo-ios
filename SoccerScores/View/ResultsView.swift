//
//  ResultsView.swift
//  SoccerScores
//
//  Created by Mateo Quiceno Sosa on 29/03/24.
//

import SwiftUI

struct ResultsView: View {
    
    let eventsTeam: [EventTeam]

    var body: some View {
        VStack {
            Text(Strings.titleResults.rawValue)
                .font(.footnote)
            
            GroupBox {
                TabView {
                    ForEach(eventsTeam, id: \.self) { eventTeam in
                        NavigationLink(destination: ResultDetailView(eventTeam: eventTeam)) {
                            ResultsCardView(eventTeam: eventTeam)
                        }
                    }
                }
                .tabViewStyle(PageTabViewStyle())
                .frame(height: 150)
            }
        }
    }
}

// MARK: - PREVIEW
struct ResultsView_Previews: PreviewProvider {
  static var previews: some View {
      ResultsView(eventsTeam: eventTeamData)
  }
}
