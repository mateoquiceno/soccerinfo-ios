//
//  TeamDetailView.swift
//  SoccerScores
//
//  Created by Mateo Quiceno Sosa on 28/03/24.
//

import SwiftUI
import CoreData

struct TeamDetailView: View {
    
    let team: Team
    
    @Environment(\.managedObjectContext) private var viewContext
    @State private var itemTeamEntity: TeamEntity?
    @State private var isTaskExecuted = false
    
    @ObservedObject private var viewModel = DetailViewViewModel()
    
    @ObservedObject private var imageLoader = ImageLoader()
    @State private var image: UIImage = UIImage()
    @ObservedObject private var imageLoaderBadge = ImageLoader()
    @State private var imageBadge: UIImage = UIImage()
    @State private var isShowingSheet: Bool = false
    @State private var isTeamFavorite: Bool = false
    @State private var toolbarIcon: String = "heart"
    
    var body: some View {
        ScrollView(.vertical, showsIndicators: false) {
            VStack(alignment: .center, spacing: 20) {
                Image(uiImage: image)
                    .resizable()
                    .scaledToFit()
                    .frame(height: 100)
                    .onReceive(imageLoader.$image) { image in
                        self.image = image
                    }
                    .onAppear {
                        imageLoader.loadImage(for: itemTeamEntity?.strTeamLogo ?? String())
                    }
                    .accessibilityIdentifier(AccesibilityIdentifier.imageTeamLogo.rawValue)
                
                Image(uiImage: imageBadge)
                    .resizable()
                    .scaledToFit()
                    .frame(width: 100, height: 100)
                    .onReceive(imageLoaderBadge.$image) { image in
                        self.imageBadge = image
                    }
                    .onAppear {
                        imageLoaderBadge.loadImage(for: itemTeamEntity?.strTeamBadge ?? String())
                    }
                    .accessibilityIdentifier(AccesibilityIdentifier.imageTeamBadge.rawValue)
                
                ResultsView(eventsTeam: viewModel.eventsTeamData)
                    .padding(.horizontal)
                    .accessibilityIdentifier(AccesibilityIdentifier.resultsView.rawValue)
                
                StadiumDetailView(itemTeamEntity: itemTeamEntity)
                    .padding(.horizontal)
                    .accessibilityIdentifier(AccesibilityIdentifier.stadiumDetail.rawValue)
                
                Button {
                    isShowingSheet.toggle()
                } label: {
                    Text(Strings.buttonMoreAbout.rawValue + (itemTeamEntity?.strTeam ?? String()))
                        .font(.title3)
                        .foregroundColor(Color(UIColor.label))
                        .bold()
                        .padding()
                }
                .background(
                    LinearGradient(
                        colors: [
                            Color(hex: itemTeamEntity?.strKitColour1 ?? "#808080"),
                            Color(hex: itemTeamEntity?.strKitColour2 ?? "#808080")
                        ],
                        startPoint: .top,
                        endPoint: .bottom
                    )
                )
                .cornerRadius(8)
                .sheet(isPresented: $isShowingSheet) {
                    AboutTeamView(itemTeamEntity: itemTeamEntity)
                }
                .accessibilityIdentifier(AccesibilityIdentifier.moreAboutTeam.rawValue)
                
                SocialNetworksView(itemTeamEntity: itemTeamEntity)
                    .padding(.horizontal)
                
            }
            .navigationBarTitle(itemTeamEntity?.strTeam ?? String(), displayMode: .inline)
            .toolbar {
                ToolbarItem(placement: .navigationBarTrailing) {
                    HStack(spacing: 16) {
                        Button(action: {
                            if !isTeamFavorite {
                                isTeamFavorite = true
                                toolbarIcon = "heart.fill"
                            } else {
                                isTeamFavorite = false
                                toolbarIcon = "heart"
                            }
                            itemTeamEntity?.isFavorite.toggle()
                            saveTeamFavoriteContext()
                        }) {
                            Image(systemName: toolbarIcon)
                                .font(.title2)
                                .foregroundColor( .accentColor)
                        }
                        
                    }
                }
            }
        }
        .onAppear() {
            if !isTaskExecuted {
                Task {
                    viewModel.fetchEventsTeamList(idTeam: team.idTeam ?? String())
                }
                fetchItemEntity()
                isTaskExecuted = true
            }
        }
    }
    
    func fetchItemEntity() {
        let fetchRequest: NSFetchRequest<TeamEntity> = TeamEntity.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "idTeam == %@", team.idTeam ?? String())
        
        do {
            let results = try viewContext.fetch(fetchRequest)
            itemTeamEntity = results.first
            validateItemFavorite()
        } catch {
            let nsError = error as NSError
            fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
        }
    }
    
    func validateItemFavorite() {
        isTeamFavorite = itemTeamEntity?.isFavorite ?? false
        if isTeamFavorite {
            toolbarIcon = "heart.fill"
        } else {
            toolbarIcon = "heart"
        }
    }
    
    private func saveTeamFavoriteContext() {
        do {
            try viewContext.save()
        } catch {
            let nsError = error as NSError
            fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
        }
    }
}
