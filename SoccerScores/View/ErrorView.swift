//
//  ErrorView.swift
//  SoccerScores
//
//  Created by Mateo Quiceno Sosa on 31/03/24.
//

import SwiftUI

struct ErrorView: View {
    var body: some View {
        VStack {
            Image(systemName: "exclamationmark.warninglight")
                .resizable()
                .frame(width: 150, height: 150)
            Text(Strings.textError.rawValue)
                .font(.title3)
                .padding()
        }
    }
}

#Preview {
    ErrorView()
}
