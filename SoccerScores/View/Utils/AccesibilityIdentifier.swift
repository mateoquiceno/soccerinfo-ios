//
//  AccesibilityIdentifier.swift
//  SoccerScores
//
//  Created by Mateo Quiceno Sosa on 31/03/24.
//

import Foundation

public enum AccesibilityIdentifier: String {
    
    //MARK: LeaguesView
    case nameLeague,
         nameLeagueAlternate,
         selectLeague,
         
         //MARK: TeamsView
         listTeams,
         gridViewTeams,
         buttonListTeams,
         buttonGridTeams,
         nameTeamList,
         
         //MARK: TeamDetailView
         imageTeamLogo,
         imageTeamBadge,
         resultsView,
         moreAboutTeam,
         
         //MARK: StadiumDetailView
         stadiumDetail,
         stadiumThumb,
         stadiumLocation,
         stadiumCapactiy,
         stadiumDescription,
         
         //MARK: AboutTeamView
         keywordsTeam,
         descriptionTeam,

         //MARK: ResultDetailView
         eventThumb,
         eventVideo
}
