//
//  Strings.swift
//  SoccerScores
//
//  Created by Mateo Quiceno Sosa on 31/03/24.
//

import Foundation

public enum Strings: String {
    
    //MARK: LeaguesView
    case buttonSelect = "Select",
         
         //MARK: TeamsView
         toolbarNavigation = "Select league",
         
         //MARK: FavoriteTeamsView
         titleFavorites = "Favorites",
         textFavoritesEmpty = "No Favorites",
         
         //MARK: TeamDetailView
         buttonMoreAbout = "More about ",
         
         //MARK: StadiumDetailView
         titleStadium = "Stadium",
         textLocationStadium = "Location: ",
         textCapacityStadium = "Capacity: ",
         
         //MARK: AboutTeamView
         textFounded = "Founded in ",
         
         //MARK: ResultDetailView
         titleResults = "Last results at home",
         textResumen = "Resumen",
         
         //MARK: SocialNetworksView
         textSocialNetworkInstagram = "Instagram",
         textSocialNetworkFacebook = "Facebook",
         textSocialNetworkX = "X",
         textSocialNetworkYoutube = "Youtube",
         textProtocol = "https://",
         
         //MARK: ErrorView
         textError = "An error occurred. Please try again later."
}
