//
//  ResultsCardView.swift
//  SoccerScores
//
//  Created by Mateo Quiceno Sosa on 29/03/24.
//

import SwiftUI

struct ResultsCardView: View {
    
    let eventTeam: EventTeam
    
    @ObservedObject private var imageLoaderHomeTeam = ImageLoader()
    @State private var imageHomeTeam: UIImage = UIImage()
    @ObservedObject private var imageLoaderAwayTeam = ImageLoader()
    @State private var imageAwayTeam: UIImage = UIImage()
    
    var body: some View {
        
        VStack {
            Text(eventTeam.strLeague ?? String())
                .font(.footnote)
                .bold()
                .foregroundColor(Color(UIColor.label))

            HStack {
                Text(eventTeam.strHomeTeam ?? String())
                    .font(.footnote)
                    .foregroundColor(Color(UIColor.label))
                
                Image(uiImage: imageHomeTeam)
                    .resizable()
                    .frame(width: 30, height: 30)
                    .onReceive(imageLoaderHomeTeam.$image) { image in
                        self.imageHomeTeam = image
                    }
                    .onAppear {
                        imageLoaderHomeTeam.loadImage(for: eventTeam.strHomeTeamBadge ?? String())
                    }
                
                Text("\(eventTeam.intHomeScore ?? String()) - \(eventTeam.intAwayScore ?? String())")
                    .font(.footnote)
                    .foregroundColor(Color(UIColor.label))

                Image(uiImage: imageAwayTeam)
                    .resizable()
                    .frame(width: 30, height: 30)
                    .onReceive(imageLoaderAwayTeam.$image) { image in
                        self.imageAwayTeam = image
                    }
                    .onAppear {
                        imageLoaderAwayTeam.loadImage(for: eventTeam.strAwayTeamBadge ?? String())
                    }
                
                Text(eventTeam.strAwayTeam ?? String())
                    .font(.footnote)
                    .foregroundColor(Color(UIColor.label))
            }
            
            Text("\(eventTeam.dateEvent ?? String()) - \(eventTeam.strTime ?? String())" )
                .font(.footnote)
                .foregroundColor(Color(UIColor.label))

        }
    }
}

// MARK: - PREVIEW

struct ResultsCardView_Previews: PreviewProvider {
  
  static var previews: some View {
      ResultsCardView(eventTeam: eventTeamData[0])
  }
}

