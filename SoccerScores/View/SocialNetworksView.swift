//
//  SocialNetworksView.swift
//  SoccerScores
//
//  Created by Mateo Quiceno Sosa on 29/03/24.
//

import SwiftUI
import CoreData

struct SocialNetworksView: View {
    
    let itemTeamEntity: TeamEntity?

    var body: some View {
        GroupBox {
            if let instagram = itemTeamEntity?.strInstagram {
                SocialNetworkDetailView(socialNetwork: instagram, nameSocialNetwork: Strings.textSocialNetworkInstagram.rawValue, imageSocialNetwork: "instagram")
            }
            if let facebook = itemTeamEntity?.strFacebook {
                SocialNetworkDetailView(socialNetwork: facebook, nameSocialNetwork: Strings.textSocialNetworkFacebook.rawValue, imageSocialNetwork: "facebook")
            }
            if let x = itemTeamEntity?.strTwitter {
                SocialNetworkDetailView(socialNetwork: x, nameSocialNetwork: Strings.textSocialNetworkX.rawValue, imageSocialNetwork: "x")
            }
            if let youtube = itemTeamEntity?.strYoutube {
                SocialNetworkDetailView(socialNetwork: youtube, nameSocialNetwork: Strings.textSocialNetworkYoutube.rawValue, imageSocialNetwork: "youtube")
            }
        }
    }
}

struct SocialNetworkDetailView: View {
    let socialNetwork: String
    let nameSocialNetwork: String
    let imageSocialNetwork: String

    var body: some View {
        
        VStack {
            HStack {
                Image(imageSocialNetwork)
                    .resizable()
                    .foregroundColor(Color(UIColor.label))
                    .frame(width: 20, height: 20)
                
                Link(nameSocialNetwork, destination: (URL(string: Strings.textProtocol.rawValue + (socialNetwork)) ?? URL(string: String()))!)
                    .font(.footnote)
                    .foregroundColor(Color(UIColor.label))
                
                Spacer()
            }
            Divider()
        }
    }
}
