//
//  LeaguesRepository.swift
//  SoccerScores
//
//  Created by Mateo Quiceno Sosa on 31/03/24.
//

import Foundation
import Combine

public protocol LeaguesRepositoryProtocol {
    func fetchLeaguesList() -> AnyPublisher<LeaguesList, Error>
}

public class LeaguesRepository: LeaguesRepositoryProtocol {
    
    public func fetchLeaguesList() -> AnyPublisher<LeaguesList, Error> {
        
        let url = URL(string: "https://www.thesportsdb.com/api/v1/json/3/all_leagues.php")!
        
        return URLSession.shared.dataTaskPublisher(for: url)
            .map { $0.data }
            .decode(type: LeaguesList.self, decoder: JSONDecoder())
            .eraseToAnyPublisher()
    }
}
