//
//  EventsRepository.swift
//  SoccerScores
//
//  Created by Mateo Quiceno Sosa on 31/03/24.
//

import Foundation
import Combine

public protocol EventsRepositoryProtocol {
    func fetchEventsTeamList(idTeam: String) -> AnyPublisher<EventsTeamList, Error>
}

public class EventsRepository: EventsRepositoryProtocol {
    
    public func fetchEventsTeamList(idTeam: String) -> AnyPublisher<EventsTeamList, Error> {
        
        var urlComponents = URLComponents(string: "https://www.thesportsdb.com/api/v1/json/3/eventslast.php")!
        let parameters: [String: String] = [
            "id": idTeam
        ]
        var queryItems = [URLQueryItem]()
        for (key, value) in parameters {
            queryItems.append(URLQueryItem(name: key, value: value))
        }
        urlComponents.queryItems = queryItems
        
        let url = urlComponents.url!
        
        return URLSession.shared.dataTaskPublisher(for: url)
            .map { $0.data }
            .decode(type: EventsTeamList.self, decoder: JSONDecoder())
            .eraseToAnyPublisher()
    }
}
