//
//  TeamsRepository.swift
//  SoccerScores
//
//  Created by Mateo Quiceno Sosa on 31/03/24.
//

import Foundation
import Combine

public protocol TeamsRepositoryProtocol {
    func fetchTeamsList(strLeague: String) -> AnyPublisher<TeamsList, Error>
}

public class TeamsRepository: TeamsRepositoryProtocol {
    
    public func fetchTeamsList(strLeague: String) -> AnyPublisher<TeamsList, Error> {
        
        var urlComponents = URLComponents(string: "https://www.thesportsdb.com/api/v1/json/3/search_all_teams.php")!
        let parameters: [String: String] = [
            "l": strLeague
        ]
        var queryItems = [URLQueryItem]()
        for (key, value) in parameters {
            queryItems.append(URLQueryItem(name: key, value: value))
        }
        urlComponents.queryItems = queryItems
        
        let url = urlComponents.url!
        
        return URLSession.shared.dataTaskPublisher(for: url)
            .map { $0.data }
            .decode(type: TeamsList.self, decoder: JSONDecoder())
            .eraseToAnyPublisher()
    }
}
