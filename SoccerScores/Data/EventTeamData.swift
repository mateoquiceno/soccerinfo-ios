//
//  EventTeamData.swift
//  SoccerScores
//
//  Created by Mateo Quiceno Sosa on 29/03/24.
//

let eventTeamData: [EventTeam] = [
    EventTeam(id: 1, idEvent: "1965701", strEvent: "Liverpool vs Sparta Prague", strLeague: "UEFA Europa League", strSeason: "2023-2024", strHomeTeam: "Liverpool", strAwayTeam: "Sparta Prague", intHomeScore: "6", intAwayScore: "1", dateEvent: "2024-03-14", strTime: "20:00:00", strHomeTeamBadge: "https://www.thesportsdb.com/images/media/team/badge/uvxuqq1448813372.png", strAwayTeamBadge: "https://www.thesportsdb.com/images/media/team/badge/zs29io1622562651.png", strThumb: "https://www.thesportsdb.com/images/media/event/thumb/kau1sa1709711062.jpg", strVideo: "https://www.youtube.com/watch?v=0N5irSbefyc")
]
