//
//  DetailViewViewModel.swift
//  SoccerScores
//
//  Created by Mateo Quiceno Sosa on 28/03/24.
//

import Foundation
import Combine

class DetailViewViewModel: ObservableObject {
    
    private var repository: EventsRepositoryProtocol = EventsRepository()
    private var disposables = Set<AnyCancellable>()
    
    @Published var eventsTeamData = [EventTeam]()
    @Published var error: Error?
    @Published var isLoading = false
    
    func fetchEventsTeamList(idTeam: String) {
        guard !isLoading else { return }
        isLoading = true
        
        repository.fetchEventsTeamList(idTeam: idTeam)
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { [weak self] completion in
                switch completion {
                case .failure(let error):
                    self?.error = error
                case .finished:
                    break
                }
                self?.isLoading = false
            }) { result in
                DispatchQueue.main.async {
                    for event in result.results ?? [EventTeam]() {
                        self.eventsTeamData.append(event)
                    }
                }
            }
            .store(in: &disposables)
    }
}
