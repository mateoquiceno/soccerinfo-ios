//
//  LeaguesViewViewModel.swift
//  SoccerScores
//
//  Created by Mateo Quiceno Sosa on 28/03/24.
//

import Foundation
import Combine

class LeaguesViewViewModel: ObservableObject {
    
    private var repository: LeaguesRepositoryProtocol = LeaguesRepository()
    private var disposables = Set<AnyCancellable>()
    
    @Published var leaguesData = [League]()
    @Published var error: Error?
    @Published var isLoading = false
    
    func fetchLeaguesList() {
        guard !isLoading else { return }
        isLoading = true
        
        repository.fetchLeaguesList()
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { [weak self] completion in
                switch completion {
                case .failure(let error):
                    self?.error = error
                case .finished:
                    break
                }
                self?.isLoading = false
            }) { result in
                DispatchQueue.main.async {
                    for league in result.leagues ?? [League]() {
                        if league.strSport == "Soccer" {
                            self.leaguesData.append(league)
                        }
                    }
                }
            }
            .store(in: &disposables)
    }
}
