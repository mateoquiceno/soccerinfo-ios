//
//  TeamsViewViewModel.swift
//  SoccerScores
//
//  Created by Mateo Quiceno Sosa on 28/03/24.
//

import Foundation
import Combine

class TeamsViewViewModel: ObservableObject {
    
    private var repository: TeamsRepositoryProtocol = TeamsRepository()
    private var disposables = Set<AnyCancellable>()
    
    @Published var teamsData = [Team]()
    @Published var error: Error?
    @Published var isLoading = false
    
    func fetchTeamsList(strLeague: String) {
        guard !isLoading else { return }
        isLoading = true
        
        repository.fetchTeamsList(strLeague: strLeague)
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { [weak self] completion in
                switch completion {
                case .failure(let error):
                    self?.error = error
                case .finished:
                    break
                }
                self?.isLoading = false
            }) { result in
                DispatchQueue.main.async {
                    for team in result.teams ?? [Team]() {
                        self.teamsData.append(team)
                    }
                }
            }
            .store(in: &disposables)
    }
}
