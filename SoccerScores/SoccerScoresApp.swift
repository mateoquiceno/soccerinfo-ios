//
//  SoccerScoresApp.swift
//  SoccerScores
//
//  Created by Mateo Quiceno Sosa on 28/03/24.
//

import SwiftUI

@main
struct SoccerScoresApp: App {
    @AppStorage("leagueSelected") var leagueSelected: String = String()
    @State private var isLinkActive: Bool = false

    let persistenceController = PersistenceController.shared

    var body: some Scene {
        WindowGroup {
            if leagueSelected.isEmpty {
                LeaguesView(isActive: $isLinkActive)
                    .environment(\.managedObjectContext, persistenceController.container.viewContext)
            } else {
                MainView()
                    .environment(\.managedObjectContext, persistenceController.container.viewContext)
            }
           
        }
    }
}
