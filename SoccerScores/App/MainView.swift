//
//  MainView.swift
//  SoccerScores
//
//  Created by Mateo Quiceno Sosa on 28/03/24.
//

import SwiftUI

struct MainView: View {
    var body: some View {
        TabView {
            TeamsView()
                .tabItem {
                    Image(systemName: "sportscourt")
                    Text("Teams")
                }
            
            FavoriteTeamsView()
                .tabItem {
                    Image(systemName: "star")
                    Text("Favorites")
                }
        }
    }
}

#Preview {
    MainView()
}
