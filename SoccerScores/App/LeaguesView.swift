//
//  LeaguesView.swift
//  SoccerScores
//
//  Created by Mateo Quiceno Sosa on 28/03/24.
//

import SwiftUI

struct LeaguesView: View {
    
    @ObservedObject var viewModel = LeaguesViewViewModel()
    @Binding var isActive: Bool

    var body: some View {
        TabView {
            if viewModel.error != nil {
                ErrorView()
            } else {
                if viewModel.isLoading {
                    ProgressView()
                } else {
                    ForEach(viewModel.leaguesData, id: \.idLeague, content: { league in
                        LeagueCardView(league: league, isActive: $isActive)
                    })
                }
            }
        }
        .tabViewStyle(PageTabViewStyle())
        .padding(.vertical, 20)
        .onAppear() {
            Task {
                viewModel.leaguesData.removeAll()
                viewModel.fetchLeaguesList()
            }
        }
        .onDisappear() {
            isActive = false
        }
    }
}
