//
//  TeamsView.swift
//  SoccerScores
//
//  Created by Mateo Quiceno Sosa on 28/03/24.
//

import SwiftUI

struct TeamsView: View {
    
    @AppStorage("leagueSelected") var leagueSelected: String?
    @ObservedObject var viewModel = TeamsViewViewModel()
    @State private var isGridViewActive: Bool = false
    @State private var isTaskExecuted = false
    @State private var isShowingSheet: Bool = false
    @State private var isLinkActive: Bool = false
    @State private var league: String?

    @Environment(\.managedObjectContext) private var viewContext

    @FetchRequest(
        sortDescriptors: [NSSortDescriptor(keyPath: \TeamEntity.idTeam, ascending: true)],
        animation: .default)
    private var itemsTeam: FetchedResults<TeamEntity>
    
    var body: some View {
        NavigationView {
            Group {
                if viewModel.error != nil {
                    ErrorView()
                } else {
                    if viewModel.isLoading {
                        ProgressView()
                    } else {
                        if isGridViewActive {
                            ScrollView(.vertical, showsIndicators: false) {
                                LazyVGrid(columns: Array(repeating: .init(.flexible()), count: 2), alignment: .center, spacing: 10) {
                                    ForEach(viewModel.teamsData, id: \.self) { team in
                                        NavigationLink(destination: TeamDetailView(team: team)) {
                                            let _ = saveTeamEntity(team: team)
                                            TeamGridItemView(team: team)
                                        }
                                    }
                                }
                                .accessibilityIdentifier(AccesibilityIdentifier.gridViewTeams.rawValue)
                                .padding(10)
                            }
                        } else {
                            List {
                                ForEach(viewModel.teamsData, id: \.self) { team in
                                    NavigationLink(destination: TeamDetailView(team: team)) {
                                        let _ = saveTeamEntity(team: team)
                                        TeamListItemView(team: team)
                                    }
                                }
                            }
                            .accessibilityIdentifier(AccesibilityIdentifier.listTeams.rawValue)
                        }
                    }
                }
            }
            .navigationBarTitle(leagueSelected ?? String(), displayMode: .large)
            .toolbar {
                ToolbarItem(placement: .navigationBarTrailing) {
                    HStack(spacing: 16) {
                        
                        Button(action: {
                            isGridViewActive = false
                        }) {
                            Image(systemName: "square.fill.text.grid.1x2")
                                .font(.title2)
                                .foregroundColor(isGridViewActive ? Color(UIColor.label) : .accentColor)
                        }
                        .accessibilityIdentifier(AccesibilityIdentifier.buttonListTeams.rawValue)
                        
                        Button(action: {
                            isGridViewActive = true
                        }) {
                            Image(systemName: "square.grid.2x2")
                                .font(.title2)
                                .foregroundColor(isGridViewActive ? .accentColor : Color(UIColor.label))
                        }
                        .accessibilityIdentifier(AccesibilityIdentifier.buttonGridTeams.rawValue)
                    }
                }
                ToolbarItem(placement: .navigationBarLeading) {
                    NavigationLink(destination: LeaguesView(isActive: $isLinkActive), isActive: $isLinkActive) {
                        Text(Strings.toolbarNavigation.rawValue)
                            .font(.footnote)
                    }
                }
            }.onAppear() {
                Task {
                    if !isTaskExecuted || league != leagueSelected {
                        league = leagueSelected
                        viewModel.teamsData.removeAll()
                        viewModel.fetchTeamsList(strLeague: leagueSelected ?? String())
                        isTaskExecuted = true
                    }
                }
            }
        }
    }
    
    private func saveTeamEntity(team: Team) {
        withAnimation {
            DispatchQueue.main.async {
                let teamItem = TeamEntity(context: viewContext)
                teamItem.idTeam = team.idTeam
                teamItem.intFormedYear = team.intFormedYear
                teamItem.strDescriptionEN = team.strDescriptionEN
                teamItem.strFacebook = team.strFacebook
                teamItem.strInstagram = team.strInstagram
                teamItem.strKeywords = team.strKeywords
                teamItem.strKitColour1 = team.strKitColour1
                teamItem.strKitColour2 = team.strKitColour2
                teamItem.strTeam = team.strTeam
                teamItem.strTeamBadge = team.strTeamBadge
                teamItem.strTeamJersey = team.strTeamJersey
                teamItem.strTeamLogo = team.strTeamLogo
                teamItem.strTwitter = team.strTwitter
                teamItem.strYoutube = team.strYoutube
                teamItem.strStadium = team.strStadium
                teamItem.strStadiumThumb = team.strStadiumThumb
                teamItem.strStadiumDescription = team.strStadiumDescription
                teamItem.strStadiumLocation = team.strStadiumLocation
                teamItem.intStadiumCapacity = team.intStadiumCapacity
                do {
                    try viewContext.save()
                } catch {
                    let nsError = error as NSError
                    fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
                }
            }
        }
    }
}
