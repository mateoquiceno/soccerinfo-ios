//
//  FavoriteTeamsView.swift
//  SoccerScores
//
//  Created by Mateo Quiceno Sosa on 29/03/24.
//

import SwiftUI
import CoreData

struct FavoriteTeamsView: View {
    
    @Environment(\.managedObjectContext) private var viewContext

    @FetchRequest(entity: TeamEntity.entity(), sortDescriptors: [], predicate: NSPredicate(format: "isFavorite == %@", NSNumber(value: true)))
    private var favoriteTeams: FetchedResults<TeamEntity>
        
    var body: some View {
        NavigationView {
            Group {
                if favoriteTeams.isEmpty {
                    VStack {
                        Image(systemName: "figure.soccer")
                            .resizable()
                            .frame(width: 200, height: 200)
                        Text(Strings.textFavoritesEmpty.rawValue)
                            .font(.largeTitle)
                    }
                } else {
                    List {
                        ForEach(favoriteTeams) { item in
                            let itemConverted = convertEntityToModel(favoriteTeamItem: item)
                            NavigationLink(destination: TeamDetailView(team: itemConverted)) {
                                TeamListItemView(team: itemConverted)
                            }
                        }
                        .onDelete(perform: deleteItems)
                    }
                }
            }
            .navigationBarTitle(Strings.titleFavorites.rawValue, displayMode: .large)
            .toolbar {
                ToolbarItem(placement: .navigationBarTrailing) {
                    EditButton()
                }
            }
        }
    }
    
    private func convertEntityToModel(favoriteTeamItem: TeamEntity) -> Team {
        return Team(idTeam: favoriteTeamItem.idTeam, strTeam: favoriteTeamItem.strTeam, strTeamShort: String(), strAlternate: String(), intFormedYear: favoriteTeamItem.intFormedYear, strLeague: String(), strLeague2: String(), strLeague3: String(), strStadium: favoriteTeamItem.strStadium, strStadiumThumb: favoriteTeamItem.strStadiumThumb, strStadiumDescription: favoriteTeamItem.strStadiumDescription, strStadiumLocation: favoriteTeamItem.strStadiumLocation, intStadiumCapacity: favoriteTeamItem.intStadiumCapacity, strWebsite: String(), strFacebook: favoriteTeamItem.strFacebook, strTwitter: favoriteTeamItem.strTwitter, strInstagram: favoriteTeamItem.strInstagram, strYoutube: favoriteTeamItem.strYoutube, strDescriptionEN: favoriteTeamItem.strDescriptionEN, strKitColour1: favoriteTeamItem.strKitColour1, strKitColour2: favoriteTeamItem.strKitColour2, strCountry: String(), strTeamBadge: favoriteTeamItem.strTeamBadge, strTeamLogo: favoriteTeamItem.strTeamLogo, strTeamJersey: favoriteTeamItem.strTeamJersey, strKeywords: favoriteTeamItem.strKeywords)
    }

    private func deleteItems(offsets: IndexSet) {
        withAnimation {
            offsets.map { favoriteTeams[$0] }.forEach { (item) in
                item.isFavorite = false
                viewContext.delete(item)
            }

            do {
                try viewContext.save()
            } catch {
                let nsError = error as NSError
                fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
            }
        }
    }
}
