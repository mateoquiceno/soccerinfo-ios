//
//  EventsTeamList.swift
//  SoccerScores
//
//  Created by Mateo Quiceno Sosa on 28/03/24.
//

import Foundation

public struct EventsTeamList: Codable {
    let results: [EventTeam]?
}
