//
//  EventTeam.swift
//  SoccerScores
//
//  Created by Mateo Quiceno Sosa on 28/03/24.
//

import Foundation

struct EventTeam: Codable, Identifiable, Hashable {
    let id: Int?
    let idEvent: String?
    let strEvent: String?
    let strLeague: String?
    let strSeason: String?
    let strHomeTeam: String?
    let strAwayTeam: String?
    let intHomeScore: String?
    let intAwayScore: String?
    let dateEvent: String?
    let strTime: String?
    let strHomeTeamBadge: String?
    let strAwayTeamBadge: String?
    let strThumb: String?
    let strVideo: String?
}

