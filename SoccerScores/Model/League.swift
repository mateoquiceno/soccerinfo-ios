//
//  League.swift
//  SoccerScores
//
//  Created by Mateo Quiceno Sosa on 28/03/24.
//

import Foundation

struct League: Codable, Identifiable, Hashable {
    var id: Int?
    var idLeague: String?
    var strLeague: String?
    var strLeagueAlternate: String?
    var strSport: String?
}
