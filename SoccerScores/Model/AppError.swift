//
//  AppError.swift
//  SoccerScores
//
//  Created by Mateo Quiceno Sosa on 28/03/24.
//

import Foundation

enum AppError: Error {
    case serverError
    case noData
    case general
}
