//
//  Team.swift
//  SoccerScores
//
//  Created by Mateo Quiceno Sosa on 28/03/24.
//

import Foundation

struct Team: Codable, Identifiable, Hashable {
    var id: Int?
    let idTeam: String?
    let strTeam: String?
    let strTeamShort: String?
    let strAlternate: String?
    let intFormedYear: String?
    let strLeague: String?
    let strLeague2: String?
    let strLeague3: String?
    let strStadium: String?
    let strStadiumThumb: String?
    let strStadiumDescription: String?
    let strStadiumLocation: String?
    let intStadiumCapacity: String?
    let strWebsite: String?
    let strFacebook: String?
    let strTwitter: String?
    let strInstagram: String?
    let strYoutube: String?
    let strDescriptionEN: String?
    let strKitColour1: String?
    let strKitColour2: String?
    let strCountry: String?
    let strTeamBadge: String?
    let strTeamLogo: String?
    let strTeamJersey: String?
    let strKeywords: String?
}
