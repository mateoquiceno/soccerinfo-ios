//
//  YouTubePlayer.swift
//  SoccerScores
//
//  Created by Mateo Quiceno Sosa on 29/03/24.
//

import SwiftUI
import WebKit

struct YouTubePlayer: UIViewRepresentable {
    let url: String

    func makeUIView(context: Context) -> WKWebView {
        let webView = WKWebView()
        if let url = URL(string: url) {
            webView.load(URLRequest(url: url))
        }
        return webView
    }

    func updateUIView(_ uiView: WKWebView, context: Context) {}
}
