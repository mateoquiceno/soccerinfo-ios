//
//  SoccerScoresTests.swift
//  SoccerScoresTests
//
//  Created by Mateo Quiceno Sosa on 28/03/24.
//

import XCTest
@testable import SoccerScores

final class SoccerScoresTests: XCTestCase {
    
    var teamsViewViewModel = TeamsViewViewModel()
    var leaguesViewViewModel = LeaguesViewViewModel()
    var detailViewViewModel = DetailViewViewModel()

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testFetchTeamsDataIsSuccess() async throws {
        // Given
        let teams = teamData

        // When
        teamsViewViewModel.teamsData = teams
        teamsViewViewModel.fetchTeamsList(strLeague: "Spanish La Liga")
        
        // Then
        XCTAssertEqual(teamsViewViewModel.teamsData.isEmpty, teams.isEmpty)
    }
    
    func testFetchLeaguesDataIsSuccess() async throws {
        // Given
        let leagues = leagueData

        // When
        leaguesViewViewModel.leaguesData = leagues
        leaguesViewViewModel.fetchLeaguesList()
        
        // Then
        XCTAssertEqual(leaguesViewViewModel.leaguesData.isEmpty, leagues.isEmpty)
    }
    
    func testFetchEventsTeamDataIsSuccess() async throws {
        // Given
        let events = eventTeamData

        // When
        detailViewViewModel.eventsTeamData = events
        detailViewViewModel.fetchEventsTeamList(idTeam: "123")
        
        // Then
        XCTAssertEqual(detailViewViewModel.eventsTeamData.isEmpty, events.isEmpty)
    }
}
